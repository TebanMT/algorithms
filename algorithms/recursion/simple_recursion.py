# Copyright Esteban Mendiola Tellez All Rights Reserved.
#
# Use of this source code is governed by an MIT-style license that can be
# found in the LICENSE file at root of this proyect or in
# https://gitlab.com/TebanMT/algorithms/-/blob/master/LICENSE
# ==============================================================================


"""
Aoutor : Esteban Mendiola Téllez
Contact: Mendiola_esteban@outlook.com

DESC:
    Here are simple examples of recursion such find an element in a list or nested list.
    These examples are simple exercises becouse don´t need memoization and work
    under simple data structs."""

def is_in(l, ele):
    """Search recursivly a element in a list. The list
    Can be nested.

    Params
    ----------------

    l -> list
        The list (nested or not) where the search occurs.

    ele -> any
        The element to search.

    Returns
    ---------------------

    True/False -> boolean
        True is the element exists, False otherwise
    """
    if l==[]:
        return False
    if type(l[0]) == list and is_in(l[0], ele):
        return True
    elif l[0] == ele:
        return True
    return is_in(l[1:], ele)

def maximun(l, max_ele=0):
    """Search recursivly the max element in a list. The list
    Can be nested.

    Params
    ----------------

    l -> list
        The list (nested or not) where the search occurs.

    Returns
    ---------------------

    max -> number
        The max number in the list
    """
    if l == []:
        return max_ele
    if type(l[0]) == list:
        max_ele = maximun(l[0], max_ele)
    elif l[0] > max_ele:
        max_ele = l[0]
    return maximun(l[1:], max_ele)

def aplanar(l):
    """'Flat' a list, if a list is neted, then the result is a simple list (no nested).
    Params
    ----------------

    l -> list
        The list (nested or not).

    Returns
    ---------------------

    result -> list
        The list flated.
    """
    if l == []:
        return l
    if type(l[0]) == list:
        return aplanar(l[0]) + aplanar(l[1:])
    return l[:1] + aplanar(l[1:])

def camino(l, ele, index=0):
    """Find the 'path' to reach the element. This path is a list where each
    element is the index to follow.
    
    Params
    ------------------------------

    l -> list
        The list of elements

    ele -> any
        The element to find.

    Returns
    ---------------------------

    result -> list
        The list of index."""

    if l == []:
        return []
    if type(l[0]) == list:
        aux = camino(l[0], ele, 0)
        if aux != []:
            return [index] + aux
        else:
            return camino(l[1:], ele, index+1)
    if l[0] == ele:
        return [index]
    return camino(l[1:], ele, index+1)


def symple(l):
    """Symple is a simple programing lenguaje. Only accept two key words: 'repeat' and 'write'.
    'write' prints a text. 'repeat' has two others elements: 'n' (number of repitations) and
    a code bloque, then the code is executed 'n' times

    Paramns
    -----------------

    l -> list
        Code instructions.

    Returns
    --------------------
        The code executed.

    Example
    --------------------
        # write 5 time 'hola!' after write 4 times 'Mundo'
        >> l = [['repeat',5,['write','hola!']],['repeat',4,['write','Mundo']]]
        >> symple(l)
        'hola!hola!hola!hola!hola!hola!MundoMundoMundoMundoMundo'
    """

    if l == []:
        return
    if isinstance(l[0], list):
        symple(l[0])
    if l[0] == 'write':
        _write(l[1])
    elif l[0] == 'repeat':
        _repeat(l[1],l[2])
    return symple(l[1:])

def _write(text):
    print(text, end="")

def _repeat(num, code):
    if num == 0:
        return
    if code[0] == 'write':
        _write(code[1])
    elif code[0] == 'repeat':
        _repeat(code[1], code[2])
    _repeat(num-1, code)
