class Node():

    def __init__(self, data, next = None):
        self.data = data
        self.next = next

class LinkedList():

    def __init__(self):
        self.head = None

    def append(self,data):
        node_to_insert = Node(data)
        if self.head is None:
            self.head = node_to_insert
            return
        current = self.head
        while current.next is not None:
            current = current.next
        current.next = node_to_insert

    def find(self, data):
        i = 0
        if self.head.data == data:
            return True, i
        current = self.head
        while current is not None:
            i = i +1
            if current.data == data:
                return True, i
            current = current.next
        return False, None

    def delete(self,data):
        if self.head.data == data:
            self.head = self.head.next
            return
        current = self.head
        while current.next is not None:
            if current.next.data == data:
                current.next = current.next.next
                break
            current = current.next

    def append_inicio(self, data):
        node_to_insert = Node(data, self.head)
        self.head = node_to_insert

    def print(self,):
        current = self.head
        if self.head is None:
            print("Empty List")
            return
        while current is not None:
            print(current.data)
            current = current.next